<?php

use Illuminate\Database\Seeder;
use App\models\Role;
use App\models\Resource;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $model;
    public function __construct(Role $model)
    {
        $this->model=$model;
    }

    public function run()
    {
        $this->model->truncate();

        $this->model->create(array(
            'RoleType'=>'Administrator',
        ));

        $this->model->create(array(
            'RoleType'=>'Moderator',
        ));

        $this->model->create(array(
            'RoleType'=>'Portal_User',
        ));

        $admin = $this->model->find(1);
        $numberOfResources=Resource::count();
        $array=[];
        for ($i =1 ;$i<=$numberOfResources;$i++){
            array_push($array,$i);

        }
        $admin->resources()->sync($array);

    }
}
