<?php
use App\Models\Resource;
use Illuminate\Database\Seeder;


class ResourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $model;

    /**
     * ResourceTableSeeder constructor.
     * @param $model
     */
    public function __construct(Resource $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->model->truncate();



        $this->model->create(array(
            "name"=>"login",
            "route"=>"login",
            "controller"=>"App\Http\Controllers\Auth\LoginController@showLoginForm",
            "description"=>"Access To login"
        ));

        $this->model->create(array(
            "name"=>"logout",
            "route"=>"logout",
            "controller"=>"App\Http\Controllers\Auth\LoginController@logout",
            "description"=>"Access To logout"
        ));

        $this->model->create(array(
            "name"=>"register",
            "route"=>"register",
            "controller"=>"App\Http\Controllers\Auth\RegisterController@showRegistrationForm",
            "description"=>"Access To register"
        ));

        $this->model->create(array(
            "name"=>"password.request",
            "route"=>"password.reset",
            "controller"=>"App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm",
            "description"=>"Access To passwordreset"
        ));

        $this->model->create(array(
            "name"=>"password.email",
            "route"=>"password.email",
            "controller"=>"App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail",
            "description"=>"Access To passwordemail"
        ));

        $this->model->create(array(
            "name"=>"password.reset",
            "route"=>"password.reset.{token}",
            "controller"=>"App\Http\Controllers\Auth\ResetPasswordController@showResetForm",
            "description"=>"Access To passwordreset{token}"
        ));

        $this->model->create(array(
            "name"=>"home",
            "route"=>"home",
            "controller"=>"App\Http\Controllers\HomeController@index",
            "description"=>"Access To home"
        ));

        $this->model->create(array(
            "name"=>"facebookCallback",
            "route"=>"login.facebook.callback",
            "controller"=>"App\Http\Controllers\Auth\LoginController@handleProviderCallback",
            "description"=>"Access To loginfacebookcallback"
        ));

        $this->model->create(array(
            "name"=>"for.admin",
            "route"=>"for-admin",
            "controller"=>"",
            "description"=>"Access To for-admin"
        ));

        $this->model->create(array(
            "name"=>"zombie.index",
            "route"=>"zombie",
            "controller"=>"App\Http\Controllers\conrzomn@index",
            "description"=>"Access To zombie"
        ));

        $this->model->create(array(
            "name"=>"zombie.create",
            "route"=>"zombie.create",
            "controller"=>"App\Http\Controllers\conrzomn@create",
            "description"=>"Access To zombiecreate"
        ));

        $this->model->create(array(
            "name"=>"zombie.store",
            "route"=>"zombie",
            "controller"=>"App\Http\Controllers\conrzomn@store",
            "description"=>"Access To zombie"
        ));

        $this->model->create(array(
            "name"=>"zombie.show",
            "route"=>"zombie.{zombie}",
            "controller"=>"App\Http\Controllers\conrzomn@show",
            "description"=>"Access To zombie{zombie}"
        ));

        $this->model->create(array(
            "name"=>"zombie.edit",
            "route"=>"zombie.{zombie}.edit",
            "controller"=>"App\Http\Controllers\conrzomn@edit",
            "description"=>"Access To zombie{zombie}edit"
        ));

        $this->model->create(array(
            "name"=>"zombie.update",
            "route"=>"zombie.{zombie}",
            "controller"=>"App\Http\Controllers\conrzomn@update",
            "description"=>"Access To zombie{zombie}"
        ));

        $this->model->create(array(
            "name"=>"zombie.destroy",
            "route"=>"zombie.{zombie}",
            "controller"=>"App\Http\Controllers\conrzomn@destroy",
            "description"=>"Access To zombie{zombie}"
        ));

        $this->model->create(array(
            "name"=>"test",
            "route"=>"test",
            "controller"=>"",
            "description"=>"Access To test"
        ));

        $this->model->create(array(
            "name"=>"routes.table",
            "route"=>"routes-table",
            "controller"=>"App\Http\Controllers\routeResourcetoSeed@getResourceTableSeeding",
            "description"=>"Access To routes-table"
        ));

        $this->model->create(array(
            "name"=>"error",
            "route"=>"error",
            "controller"=>"",
            "description"=>"Access To error"
        ));
    }
}
