<?php

use Illuminate\Database\Seeder;
use \App\models\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $model;

    /**
     * ResourceTableSeeder constructor.
     * @param $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function run()
    {
        $this->model->truncate();


        $this->model->create(array(
            "name"=>"Ahtisham Shahid",
            "email"=>"ahtisham300@gmail.com",
            "password"=>'$2y$10$f3wFWq4QBKoI1nO6Vw5RcO1CMmQim5AjE/CTkYG6wSiM2kJSTj6nS',
        ));
        $this->model->create(array(
            "name"=>"User",
            "email"=>"user@gmail.com",
            "password"=>'$2y$10$f3wFWq4QBKoI1nO6Vw5RcO1CMmQim5AjE/CTkYG6wSiM2kJSTj6nS',
        ));
        $this->model->create(array(
            "name"=>"Moderator",
            "email"=>"mod@gmail.com",
            "password"=>'$2y$10$f3wFWq4QBKoI1nO6Vw5RcO1CMmQim5AjE/CTkYG6wSiM2kJSTj6nS',
        ));


        $user1 = $this->model->find(1);
        $user1->roles()->sync( [
            0 => "1",
        ]);

        $user2 = $this->model->find(2);
        $user2->roles()->sync( [
            0 => "2",
        ]);

        $user3 = $this->model->find(3);
        $user3->roles()->sync( [
            0 => "3",
        ]);

    }
}
