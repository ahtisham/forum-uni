
@extends('admin.layouts.content')
@section('content')


    <div class="col-md-6">
        <select class="form-control">
            @foreach($roles as $role)
            <option>{{print_r($role->RoleType)}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-6" >
        @foreach($resources as $resource)
            <div class="checkbox">
                <label><input name="resources[]"  type="checkbox" value="{{$resource->id}}">{{$resource->name}}</label>
            </div>
            @endforeach

    </div>
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Checkable Tree
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div id="m_tree_3" class="tree-demo jstree jstree-3 jstree-default jstree-checkbox-selection" role="tree" aria-multiselectable="true" tabindex="-1" aria-activedescendant="j3_5" aria-busy="false" aria-selected="false">
                <ul class="jstree-container-ul jstree-children jstree-wholerow-ul jstree-no-dots" role="group">
                    <li role="treeitem" aria-selected="true" aria-level="1" aria-labelledby="j3_1_anchor" aria-expanded="true" id="j3_1" class="jstree-node jstree-open">
                        <div unselectable="on" role="presentation" class="jstree-wholerow jstree-wholerow-clicked">&nbsp;</div>
                        <i class="jstree-icon jstree-ocl" role="presentation"></i><a class="jstree-anchor jstree-clicked" href="#" tabindex="-1" id="j3_1_anchor"><i class="jstree-icon jstree-checkbox" role="presentation"></i><i class="jstree-icon jstree-themeicon fa fa-folder m--font-warning jstree-themeicon-custom" role="presentation"></i>Same but with checkboxes</a>
                        <ul role="group" class="jstree-children" style="">
                            <li role="treeitem" aria-selected="true" aria-level="2" aria-labelledby="j3_2_anchor" id="j3_2" class="jstree-node  jstree-leaf">
                                <div unselectable="on" role="presentation" class="jstree-wholerow jstree-wholerow-clicked">&nbsp;</div>
                                <i class="jstree-icon jstree-ocl" role="presentation"></i><a class="jstree-anchor  jstree-clicked" href="#" tabindex="-1" id="j3_2_anchor"><i class="jstree-icon jstree-checkbox" role="presentation"></i><i class="jstree-icon jstree-themeicon fa fa-folder m--font-warning jstree-themeicon-custom" role="presentation"></i>initially selected</a>
                            </li>
                            <li role="treeitem" aria-selected="true" aria-level="2" aria-labelledby="j3_3_anchor" id="j3_3" class="jstree-node  jstree-leaf">
                                <div unselectable="on" role="presentation" class="jstree-wholerow jstree-wholerow-clicked">&nbsp;</div>
                                <i class="jstree-icon jstree-ocl" role="presentation"></i><a class="jstree-anchor  jstree-clicked" href="#" tabindex="-1" id="j3_3_anchor"><i class="jstree-icon jstree-checkbox" role="presentation"></i><i class="jstree-icon jstree-themeicon fa fa-warning m--font-danger jstree-themeicon-custom" role="presentation"></i>custom icon</a>
                            </li>
                            <li role="treeitem" aria-selected="true" aria-level="2" aria-labelledby="j3_4_anchor" aria-expanded="true" id="j3_4" class="jstree-node  jstree-open">
                                <div unselectable="on" role="presentation" class="jstree-wholerow jstree-wholerow-clicked">&nbsp;</div>
                                <i class="jstree-icon jstree-ocl" role="presentation"></i><a class="jstree-anchor  jstree-clicked" href="#" tabindex="-1" id="j3_4_anchor"><i class="jstree-icon jstree-checkbox" role="presentation"></i><i class="jstree-icon jstree-themeicon fa fa-folder m--font-default jstree-themeicon-custom" role="presentation"></i>initially open</a>
                                <ul role="group" class="jstree-children">
                                    <li role="treeitem" aria-selected="true" aria-level="3" aria-labelledby="j3_5_anchor" id="j3_5" class="jstree-node  jstree-leaf jstree-last">
                                        <div unselectable="on" role="presentation" class="jstree-wholerow jstree-wholerow-clicked jstree-wholerow-hovered">&nbsp;</div>
                                        <i class="jstree-icon jstree-ocl" role="presentation"></i><a class="jstree-anchor  jstree-clicked" href="#" tabindex="-1" id="j3_5_anchor"><i class="jstree-icon jstree-checkbox" role="presentation"></i><i class="jstree-icon jstree-themeicon fa fa-folder m--font-warning jstree-themeicon-custom" role="presentation"></i>Another node</a>
                                    </li>
                                </ul>
                            </li>
                            <li role="treeitem" aria-selected="true" aria-level="2" aria-labelledby="j3_6_anchor" id="j3_6" class="jstree-node  jstree-leaf">
                                <div unselectable="on" role="presentation" class="jstree-wholerow jstree-wholerow-clicked">&nbsp;</div>
                                <i class="jstree-icon jstree-ocl" role="presentation"></i><a class="jstree-anchor  jstree-clicked" href="#" tabindex="-1" id="j3_6_anchor"><i class="jstree-icon jstree-checkbox" role="presentation"></i><i class="jstree-icon jstree-themeicon fa fa-warning m--font-waring jstree-themeicon-custom" role="presentation"></i>custom icon</a>
                            </li>
                            <li role="treeitem" aria-selected="true" aria-level="2" aria-labelledby="j3_7_anchor" aria-disabled="true" id="j3_7" class="jstree-node  jstree-leaf jstree-last">
                                <div unselectable="on" role="presentation" class="jstree-wholerow jstree-wholerow-clicked">&nbsp;</div>
                                <i class="jstree-icon jstree-ocl" role="presentation"></i><a class="jstree-anchor  jstree-clicked jstree-disabled" href="#" tabindex="-1" id="j3_7_anchor"><i class="jstree-icon jstree-checkbox" role="presentation"></i><i class="jstree-icon jstree-themeicon fa fa-check m--font-success jstree-themeicon-custom" role="presentation"></i>disabled node</a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </div>


@endsection