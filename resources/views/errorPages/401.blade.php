@extends('errorPages.layout')

@section('title', 'UnAuthorised')

@section('message', 'You are not authorised to access that resource .')
