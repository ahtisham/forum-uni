<?php
use Illuminate\Routing\Router;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');




//**facebook api for login and signuo

Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback')->name('facebookCallback');

//++++++ for role base authentication test routes



Route::middleware(['auth','have.permission'])->group(function(){


    Route::get('for-admin',function (){
        dd('dhasjhdjash');
        if (Gate::allows('admin-only')) {
            return view('adminhome');
        }else
            return 'no admin ';

    })->name('for.admin');


    Route::get('for-user',function (){
        if (Gate::allows('user-only')) {
            return view('user');
        }else
            return 'no admin ';

    });

    Route::get('for-modrator',function (){
        dd('in moderator');
    });


    Route::resource('zombie','conrzomn');

    Route::get('role-resource','RoleController@index')->name('role.resource');




});

//test route
Route::get('test',function (){
return view('admin.managerole');
})->name('test');

Route::get('routes-table','routeResourcetoSeed@getResourceTableSeeding')->name('routes.table');
Route::get('error', function (){
    return view('errorPages.401');
})->name('error');

