<?php

namespace App\Policies;

use App\models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TestPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function adminOnly($user){
        $roles =$user->roles;
        foreach ($roles as $role){
            if($role->RoleType=='admin'){
                return true;
            }
        }
        return false;

    }
    public function modOnly($user){
        $roles =$user->roles;
        foreach ($roles as $role){
            if($role->RoleType=='moderator'){
                return true;
            }
        }
        return false;

    }
    public function userOnly($user){
        $roles =$user->roles;
        foreach ($roles as $role){
            if($role->RoleType=='portal_user'){
                return true;
            }
        }
        return false;

    }

}
