<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{



    public function roles()
    {
        return $this->belongsToMany('Role');
    }

}
