<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Router;
use Illuminate\Http\Request;
use ReflectionClass;

class routeResourcetoSeed extends Controller
{

    public function getResourceTableSeeding(){


        $routes = app('router')->getRoutes();
        function accessProtected($obj, $prop) {
            $reflection = new ReflectionClass($obj);
            $property = $reflection->getProperty($prop);
            $property->setAccessible(true);
            return $property->getValue($obj);
        }


        $props=accessProtected($routes,'nameList');


        foreach ($props as $key => $prop) {
        $controller = null;
        if (isset($prop->action['controller'])) {
          $controller=$prop->action['controller'];# code...
        }
            $oneEntry='$this->model->create(array('

                .'<br>'         . '"'.'name' .'"'. '=>' . '"'.$key.'"' .  ','
                .'<br>'         .'"'.'route'.'"'. '=>' . '"'. str_replace('/','.',$prop->uri) .'"' . ','
                .'<br>'         .'"'.'controller'.'"' . '=>' . '"'.  $controller  .'"' . ','
                .'<br>'         .'"'.'description' .'"'. '=>'. '"' . 'Access To '. str_replace('/','',$prop->uri).'"'
                .'<br>'         . '));'.'<br>';

            echo '<br>'.$oneEntry;
        }

    }
}
