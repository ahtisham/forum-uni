<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use App\models\Resource;
use App\models\Role;

class VerifyRoutePermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = null;
        $requiredRoute = null;
        $roles = null;
        $resource =null;
        

        if(Auth::check()){
           // if(){};
            $user = Auth::User();
            $requiredRoute = $request->route()->getName();
            $roles = $user->roles;

            foreach ($roles as $role){
                if($role->RoleType==='Administrator'){
                    return $next($request);
                }elseif ($role->resources()->where('name', $requiredRoute)->first()) //can putt in role model
                {
                    return $next($request);
                } else {
                    return redirect()->route('error');
                }
            }
        }

    }
}
